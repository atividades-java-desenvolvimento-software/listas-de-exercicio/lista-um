import java.util.Scanner;

public class Exercicio04 {
    public void exercicio04(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Informe primeiro numero: ");
        int num1 = sc.nextInt();
        System.out.println("Informe segundo numero: ");
        int num2 = sc.nextInt();

        System.out.println("Soma: " + num1 + " + " + num2 + " = " + (num1 + num2));
        System.out.println("Subtração: " + num1 + " - " + num2 + " = " + (num1 - num2));
        System.out.println("Multiplicação: " + num1 + " * " + num2 + " = " + (num1 * num2));
        System.out.println("Divisão: " + num1 + " / " + num2 + " = " + (num1 / num2));

        sc.close();
    }
}
