import java.util.Scanner;

public class Exercicio06 {
    public void exercicio06(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Informe a temperatura em celcius: ");
        double celcius = sc.nextDouble();
        double fahrenheit = (9* celcius + 160) / 5;
        System.out.println("Graus em Celcius: " + celcius + "\nGraus em Fahrenheit: " + fahrenheit);
        sc.close();
    }
}
