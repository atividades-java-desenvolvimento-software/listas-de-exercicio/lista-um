import java.util.Scanner;

public class Exercicio08 {
    public void exercicio08(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Informe número: ");
        int numero = sc.nextInt();
        String condicao;
        // Usei a primeira vez o operador ternário  
        condicao = (numero >= 50) ? "Número maior ou igual a 50" : "Número menor que 50";
        System.out.println(condicao);

        sc.close();
    }
}
