import java.util.Scanner;

public class Exercicio17 {
    public void exercicio17(){
        Scanner sc = new Scanner(System.in);
        double nota1 = sc.nextDouble();
        double nota2 = sc.nextDouble();
        double nota3 = sc.nextDouble();
        double peso1 = sc.nextDouble();
        double peso2 = sc.nextDouble();
        double peso3 = sc.nextDouble();

        double mediaPonderada = (nota1 * peso1 + nota2 * peso2 + nota3 * peso3) / (peso1 + peso2 + peso3);
        
        System.out.println("Média Ponderada: " + mediaPonderada);
        sc.close();
    }
}
