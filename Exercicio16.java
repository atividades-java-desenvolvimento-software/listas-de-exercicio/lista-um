import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.Scanner;

public class Exercicio16 {
    public void exercicio16(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Informe nota 1: ");
        double nota1 = sc.nextDouble();
        System.out.println("Informe nota 2: ");
        double nota2 = sc.nextDouble();
        System.out.println("Informe nota 3: ");
        double nota3 = sc.nextDouble();

        double mediaAritimetica = (nota1 + nota2 + nota3) / 3;

        System.out.println("Média: " + mediaAritimetica);

        sc.close();
    }
}
