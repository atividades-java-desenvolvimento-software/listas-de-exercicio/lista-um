import java.util.Scanner;

public class Exercicio05 {
    public void exercicio05(){
        Scanner sc = new Scanner(System.in);

        System.out.println("Informe A: ");
        int a = sc.nextInt();

        System.out.println("Informe B: ");
        int b = sc.nextInt();

        System.out.println("Original: ");
        System.out.println(a);
        System.out.println(b);

        int aux = a;
        a = b;
        b = aux;

        System.out.println("Trocados: ");
        System.out.println(a);
        System.out.println(b);

        sc.close();
    }
}
