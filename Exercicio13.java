import java.util.Scanner;

public class Exercicio13 {
    public void exercicio13(){
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Informe A: ");
        double a = sc.nextDouble();

        System.out.println("Informe B: ");
        double b = sc.nextDouble();

        System.out.println("Informe operação: +, -, *, / ");
        char op = sc.next().charAt(0);

        double resultado = calcular(a,b,op);
        //MAX_VALUE É O VALOR MAXIMO QUE UM DOUBLE PODE ATINGIR NA DIVISÃO  
        if (resultado != Double.MAX_VALUE){
            System.out.println("Resultado: " + resultado);
        }else{
        }
        System.out.println("ERRO DE DIVISÃO POR ZERO OU OPERADOR NÃO DEFINIDO");

        sc.close();
    }
    public static double calcular(double a, double b, char op) {
        switch (op) {
            case '+':
                return a + b;
            case '-':
                return a - b;
            case '*':
                return a * b;
            case '/':
                return a / b;
            default:
                System.out.println("Operação inválida.");
                return op;
        }
    
    
}
     
}