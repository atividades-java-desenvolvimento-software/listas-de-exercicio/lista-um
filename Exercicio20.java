import java.util.Scanner;

public class Exercicio20 {
    public void exercicio20(){
        Scanner sc = new Scanner(System.in);

        System.out.println("Informe o tempo da viagem em horas: ");
        double tempoViagem = sc.nextDouble();

        System.out.println("Informe a velocidade em km/h: ");
        double kmHora = sc.nextDouble();

        double distancia = tempoViagem * kmHora;

        double consumo = 12;
        double quantidadeLitros = distancia/consumo;

        System.out.println("Quantidade de Litros que o combustivel gasta: " + quantidadeLitros);


        sc.close();
    }
}
