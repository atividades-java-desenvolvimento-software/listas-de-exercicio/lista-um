import java.util.Scanner;

public class Exercicio10 {
    public void exercicio10(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Informe um número de 1 a 5");
        int numero = sc.nextInt();

        switch (numero) {
            case 1:
                System.out.println("Um");
                break;
            case 2:
                System.out.println("Dois");
                break;
            case 3:
                System.out.println("Três");
                break;
            case 4:
                System.out.println("Quatro");
                break;
            case 5:
                System.out.println("Cinco");
            default:
                System.out.println("Número invalido!");
                break;
        }




        sc.close();
    }
}
