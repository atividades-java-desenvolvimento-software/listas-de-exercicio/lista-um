import java.util.Scanner;

public class Exercicio19 {
    public void exercicio19(){
        Scanner sc = new Scanner(System.in);
        //raio 10
        double raio = sc.nextDouble();
        //altura 15
        double altura = sc.nextDouble();
        // valor de pi tentei usar Math.pi, mas sobe pra 4712
        double volume = 3.14 * Math.pow(raio, 2) * altura;

        System.out.println("Volume: " + volume);

        sc.close();
    }
    
}
