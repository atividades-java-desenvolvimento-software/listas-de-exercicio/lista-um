import java.util.Scanner;
public class Exercicio03 {
    public void exercicio03(){
        Scanner sc = new Scanner(System.in); 

        System.out.println("Informe A: ");
        int a = sc.nextInt();
        System.out.println("Informe B: ");
        int b = sc.nextInt();

        if(a > b){
            System.out.println("A > B");
        }else if(a < b){
            System.out.println("A < B");
        }else{
            System.out.println("A sequência de números informados é invalida.");
        }

        sc.close();
    }
}
