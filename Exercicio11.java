import java.util.Arrays;
import java.util.Scanner;

public class Exercicio11 {
    public void exercicio11(){
        Scanner sc = new Scanner(System.in);

        System.out.println("Informe qualquer número: ");
        int num[] = new int[3];

        for(int i = 0;i < 3; i++){
            System.out.println("Digite um número: ");
            num[i] = sc.nextInt();
        }
        
        System.out.println("-------------------------");
        // Chamado da metodo Sort pra organizar

        Arrays.sort(num);
        System.out.println("Ordem crescente:");
        for(int i =0; i<3;i++){
            System.out.println(num[i]);
        }
       

        sc.close();
    }
}
