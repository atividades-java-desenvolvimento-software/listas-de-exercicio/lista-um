import java.text.DecimalFormat;
import java.util.Scanner;

public class Exercicio15 {
    public void exercicio15(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Informe x1: ");
        double x1 = sc.nextDouble();
        System.out.println("Informe y1: ");
        double y1 = sc.nextDouble();
        System.out.println("Informe x2: ");
        double x2 = sc.nextDouble();
        System.out.println("Informe y2: ");
        double y2 = sc.nextDouble();

        double d = Math.sqrt(Math.pow(x2 - x1,2) + Math.pow(y2 - y1,2));
        //uso de outras classes (Classe Math e Classe DecimalFormat)
        DecimalFormat df = new DecimalFormat("#.##");
        String distanciaFormatada = df.format(d);
        System.out.println("Distância: " + distanciaFormatada);
        
        sc.close();
    }
    
}
