import java.util.Scanner;

public class Exercicio07 {
    public void exercicio07(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Informe número: ");
        int numero = sc.nextInt();

        if(numero >= 100 && numero <= 200){
            System.out.println("Seu número está entre 100 e 200");
        }else{
            System.out.println("Número fora do intervalo entre 100 e 200");
        }

        sc.close();
    }
}
