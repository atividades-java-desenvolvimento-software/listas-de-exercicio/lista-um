import java.util.Scanner;

public class Exercicio18 {
    public void exercicio18(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Informe nota 1: ");
        double nota1 = sc.nextDouble();
        System.out.println("Informe nota 2: ");
        double nota2 = sc.nextDouble();
        System.out.println("Informe nota 3: ");
        double nota3 = sc.nextDouble();

        double mediaHarmonica = 3/(1/nota1 + 1/nota2 + 1/nota3); 
        
        System.out.println("Média: " + mediaHarmonica);

        sc.close();
    }   
    
}
