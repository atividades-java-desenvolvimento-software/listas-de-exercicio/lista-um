import java.util.Scanner;

public class Exercicio14 {
    public void exercicio14(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Informe a1: ");
        double a1 = sc.nextDouble();
        System.out.println("Informe n: ");
        double n = sc.nextDouble();
        System.out.println("Informe r: ");
        double r = sc.nextDouble();

        double An = a1 + (n - 1) * r;
        System.out.println("Resultado An = " + An);

        sc.close();
    }
}
