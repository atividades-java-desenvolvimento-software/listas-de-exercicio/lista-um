import java.util.Scanner;
public class Exercicio02 {
    public void exercicio02(){
        //soma e resultado
        Scanner sc = new Scanner(System.in);
        System.out.println("Informe primeiro número: ");
        int num1 = sc.nextInt();
        System.out.println("Informe segundo número: ");
        int num2 = sc.nextInt();
        System.out.println("Resultado de: " + num1 +" + " + num2 +" é = " + (num1 + num2));

        sc.close();
    }
}
